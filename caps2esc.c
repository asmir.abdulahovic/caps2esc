#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <linux/input.h>
#include <unistd.h>

// clang-format off
static const struct input_event
capslock_down   = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 1},
capslock_repeat = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 2},
capslock_up     = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 0},
ctrl_down       = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 1},
ctrl_repeat     = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 2},
ctrl_up         = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 0},
esc_down        = {.type = EV_KEY, .code = KEY_ESC,      .value = 1},
esc_repeat      = {.type = EV_KEY, .code = KEY_ESC,      .value = 2},
esc_up          = {.type = EV_KEY, .code = KEY_ESC,      .value = 0},
lshift_down     = {.type = EV_KEY, .code = KEY_LEFTSHIFT, .value = 1},
lshift_up       = {.type = EV_KEY, .code = KEY_LEFTSHIFT, .value = 0},
rshift_down     = {.type = EV_KEY, .code = KEY_RIGHTSHIFT, .value = 1},
rshift_up       = {.type = EV_KEY, .code = KEY_RIGHTSHIFT, .value = 0},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

static int equal(const struct input_event *first,
                 const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

static int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

static void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

static void write_esc(void) {
    usleep(20000);
    write_event(&esc_down);
    write_event(&syn);
    usleep(20000);
    write_event(&esc_up);
}

int main(void) {
    bool is_lshift_down      = false;
    bool is_rshift_down      = false;
    bool write_esc_lshift    = false;
    bool write_esc_rshift    = false;
    struct input_event input = {0};

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN) {
            continue;
        }

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        write_esc_lshift = equal(&input, &lshift_up) && is_lshift_down;
        write_esc_rshift = equal(&input, &rshift_up) && is_rshift_down;
        is_lshift_down   = equal(&input, &lshift_down);
        is_rshift_down   = equal(&input, &rshift_down);

        if (equal(&input, &capslock_down) || equal(&input, &capslock_up)) {
            input.code = KEY_LEFTMETA;
        }

        write_event(&input);

        if (write_esc_lshift) {
            write_esc_lshift = false;
            write_esc();
        }

        if (write_esc_rshift) {
            write_esc_rshift = false;
            write_esc();
        }
    }
}
