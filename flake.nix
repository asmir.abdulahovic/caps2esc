{
  description = "caps2esc nix flake";

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        pname = "caps2esc";
        version = "1.0.0";
        src = self;
        nativeBuildInputs = [ cmake ];
      };
  };
}
